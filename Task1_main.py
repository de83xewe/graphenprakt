from Task1_functions import *
from Task1_functions2 import *
from collections import Counter

path = 'C:\\Users\\dusty\\PycharmProjects\\graphenprakt\\True'
filtered_list = [file for file in fileFinder2(path, 'filtered', '.graph')]
unfiltered_list = [file for file in fileFinder2(path, 'unfiltered', '.graph')]
truths_list = [file for file in fileFinder2(path, '', '.gtf')]

outDegreeList = []
numberUnresolvedProblemNodes = []
numberOfResolvedProblemNodes = []

for path in unfiltered_list:
    with open(path, 'r') as myfile:
        data = myfile.read().replace('\n', '|')

    exons, multiBins, multiBinsPaired = getExons(data), getMultiBins(data), getMultiBinsPaired(data)

    if checkIfMultiBinsAreCorrect(multiBins, exons):
        ig_data, cg_graph, rg_graph = getIG(data), getCG(data), getRG(data)
        ig_labels, cg_labels, rg_labels = getLabels(ig_data), getLabels(cg_graph), getLabels(rg_graph)
        ig_arcs, cg_arcs, rg_arcs = getArcs(ig_data), getArcs(cg_graph), getArcs(rg_graph)

     #   translatedBins = translateBin2(exons, cg_arcs)
      #  transcript_list = extractTranscripts_list(path)

        # s_transcriptSplicingSites = extractTranscriptStringSplicingSites()
        # s_graphSplicingSites = createSplicingSiteRanges(translatedBins)
        # print(path)
        problemNodesCG = getProblemNodes(data, 'cg')
        problemNodesRG = getProblemNodes(data, 'rg')

        problemSetCG = set(problemNodesCG)
        problemSetRG = set(problemNodesRG)
        problemNodesUnresolved = problemSetCG.intersection(problemSetRG)
        problemNodesResolved = problemSetCG.symmetric_difference(problemSetRG)
        # print('RG {} and CG {}'.format(problemSetRG,problemSetCG))

        # number of nodes which couldn't be resolved from CG to RG (node which are in both)
        numberUnresolvedProblemNodes.append(len(problemNodesUnresolved))

        # number of nodes which have been resolved vom CG to RG
        numberOfResolvedProblemNodes.append(len(problemNodesResolved))

        result = calcExonDegrees(ig_arcs, 'in', problemNodesUnresolved)
        for _, degree, evidence in result:
            outDegreeList.append(evidence)
    #       for item in result:
    #           print(item)
    #       exit(1)
        result = outDegreeList
    else:
        print("damn, something is wrong here! len(Bin) != #Exons for each Bin ")

# outDegreeList = Counter(outDegreeList)
# result = []
# for key, value in zip(outDegreeList.keys(), outDegreeList.values()):
#     result.append([str(key), value])

# solvedList = Counter(numberOfResolvedProblemNodes)
# solved = []
# for key, value in zip(solvedList.keys(),solvedList.values()):
#     solved.append([str(key), value])
#
# solvedList2 = Counter(numberUnresolvedProblemNodes)
# solved2 = []
# for key, value in zip(solvedList2.keys(),solvedList2.values()):
#     solved2.append([str(key), value])

print('degrees')
print(result)
print(len(result))
#print('solved nodes')
#print(solved)
#print('unsolved nodes')
#print(solved2)

# print(outDegreeList.values(),outDegreeList.keys())
# C:\Users\dusty\PycharmProjects\graphenprakt\star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001\star_without_gene_models.bam.chr1.137949360.138130838\splice_graph
