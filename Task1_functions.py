import re
import os


def getExons(data):
    exon_list = []
    exons_tmp = re.split('Exon Lists ---(.*?)Multi Bins ---', data)
    exons = exons_tmp[1].split('|')
    for exon in exons:
        if exon != "":
            exon = exon.split(" ")
            tmp = exon[1].split("-")
            left, right = int(tmp[0]), int(tmp[1])
            exon_list.append([int(exon[0].replace("Exon", "")), left, right])
    return exon_list


def getMultiBins(data):
    multiBins_list = []
    multiBins_tmp = re.split('Multi Bins ---(.*?)Pair Bins ---', data)
    multiBins = multiBins_tmp[1].split('|')
    for multiBin in multiBins:
        if multiBin != "":
            multiBins_list.append(multiBin.split(" "))
    return multiBins_list


def getMultiBinsPaired(data):
    multiBinsPaired_list = []
    multiBinsPaired_tmp = re.split('Pair Bins ---(.*?)Initial Graph ---', data)
    multiBinsPaired = multiBinsPaired_tmp[1].split('|')
    for multiBinPaired in multiBinsPaired:
        if multiBinPaired != "":
            multiBinsPaired_list.append(multiBinPaired.split(" "))
    return multiBinsPaired_list


def checkIfMultiBinsAreCorrect(multiBins, exons):
    # idea: len of each MultiBin = #exons
    goal_len = len(exons)
    for bin in multiBins:
        if len(str((multiBins[0]))) - 4 != goal_len:
            return False
    return True


def getLabels(data):
    tmp_list = []
    tmp_tmp = re.split('label(.*?)@arcs', data.replace('\t', ''))
    tmp = tmp_tmp[1].split('|')
    for unit in tmp:
        if unit != "":
            tmp_list.append(unit)
    return tmp_list


def getArcs(data):
    tmp_list = []
    tmp_tmp = re.split('@arcs(.*?)@attributes', data.replace('\t', ' '))
    tmp = tmp_tmp[1].split('|')
    for unit in tmp:
        if unit != "":
            tmp_list.append(unit)
    return tmp_list


def getProblemNodes(data, argument):
    tmp_list, tmp_tmp = [], []
    data = data.replace('\t', ' ')
    if argument == 'cg' or argument == 'CG':
        tmp_tmp = re.split('Compacted graph ---(.*?)@arcs', data)
    elif argument == 'rg' or argument == 'RG':
        tmp_tmp = re.split('Resolved graph ---(.*?)@arcs', data)

    problemNodeRange = tmp_tmp[1].split('|')
    # problemNodeRange = ['', '@nodes', 'label ', '0 ', '1 ',...,]
    for node in problemNodeRange[3:]:  # [3:] because first three are waste
        if node != "" and int(node) >= 2:
            tmp_list.append(int(node))
    return tmp_list


def getIG(data):
    tmp_tmp = re.split('Initial Graph ---(.*?)Compacted graph ---', data)
    return tmp_tmp[1]


def getCG(data):
    tmp_tmp = re.split('Compacted graph ---(.*?)Resolved graph ---', data)
    return tmp_tmp[1]


def getRG(data):
    tmp_tmp = re.split('Resolved graph ---(.*?)drain', data)
    return tmp_tmp[1]


def calcExonDegrees(arcs, degreeType, problemNodes=None):
    nodes_1, nodes_2, nodes_4, arc_list = [], [], [], []

    for line in arcs:
        tmp = line.replace('"', '').split(" ")
        arc_list.append(tmp)
    arc_list = arc_list[1:]
    for _node_in, _node_out, _node_label, _node_bin, _node_resolve, _node_type, _node_evidence, _ in arc_list:
        _node_type = int(_node_type)
        if _node_type is 1:
            #  nodes_1.append((_node_in, _node_out))
            nodes_1.append((_node_in, _node_out, _node_evidence))
        elif _node_type is 2:
            nodes_2.append((_node_in, _node_out, _node_evidence))
        elif _node_type is 4:
            nodes_4.append((_node_in, _node_out, _node_evidence))
        else:
            print('got no type ?!')

    if len(nodes_2) >= 1:  # that's valid for InitialGraph
        nodes_right_list, nodes_left_list = [], []

        for node_left, node_right, node_evidence in nodes_1:
            nodes_right_list.append(node_right)
            nodes_left_list.append(node_left)
        for node_left, node_right, node_evidence in nodes_4:
            nodes_right_list.append(node_right)
            nodes_left_list.append(node_left)

        inDegree, outDegree = set(), set()

        for node_left, node_right, node_evidence in nodes_1 + nodes_4:
            outDegree.add((node_left, nodes_left_list.count(node_left), node_evidence))
            inDegree.add((node_right, nodes_right_list.count(node_right), node_evidence))
            if node_left in nodes_right_list:
                outDegree.add((node_left, nodes_right_list.count(node_right), node_evidence))
            elif node_right in nodes_left_list:
                inDegree.add((node_right, nodes_left_list.count(node_left), node_evidence))
            else:
                for node_left2, node_right2, node_evidence in nodes_2:
                    if node_left == node_right2:
                        inDegree.add((node_left, nodes_right_list.count(node_left2), node_evidence))
                    if node_right == node_left2:
                        outDegree.add((node_right, nodes_left_list.count(node_right2), node_evidence))
        outDegree.add(('0', nodes_left_list.count('0'), 'unknown'))
        inDegree.add(('1', nodes_right_list.count('1'), 'unknown'))

    else:  # that's valid for Resolved and compressed Graph
        nodes_right_1_list, nodes_left_1_list = [], []
        for node_left, node_right in nodes_1:
            nodes_right_1_list.append(node_right)
            nodes_left_1_list.append(node_left)
        inDegree, outDegree = set(), set()
        for node_left1, node_right1 in nodes_1:  # look at all tuples in nodes_1
            outDegree.add((node_left1, nodes_right_1_list.count(node_right1)))
            inDegree.add((node_right1, nodes_left_1_list.count(node_left1)))
        outDegree.add(('0', nodes_left_1_list.count('0')))
        inDegree.add(('1', nodes_right_1_list.count('1')))

    if problemNodes is None:
        if degreeType == 'out':
            return outDegree
        elif degreeType == 'in':
            return inDegree
    else:
        if degreeType == 'out':
            outDegreeFiltered = set()
            for node, count, node_evidence in outDegree:
                if int(node) in problemNodes:
                    outDegreeFiltered.add((node, count, node_evidence))
            return outDegreeFiltered
        elif degreeType == 'in':
            inDegreeFiltered = set()
            for node, count, node_evidence in inDegree:
                if int(node) in problemNodes:
                    inDegreeFiltered.add((node, count, node_evidence))
            return inDegreeFiltered


def getEdgesFromNode(node, arcs):
    # print('look for edges from node {}'.format(node))
    arc_list, nodes_1, nodes = [], [], []
    for line in arcs:
        tmp = line.replace('"', '').split(" ")
        arc_list.append(tmp)
    arc_list = arc_list[1:]
    for _node_in, _node_out, _node_label, _node_bin, _node_resolve, _node_type, _node_evidence, _ in arc_list:
        _node_type = int(_node_type)
        if _node_type is 1:
            nodes.append((_node_in, _node_out, _node_label))
        else:
            print('got no type ?!')

    nodes_right_list, nodes_left_list = [], []
    for node_left, node_right, node_label in nodes:
        if int(node_left) == node:
            nodes_right_list.append([int(node_left), int(node_label), int(node_right)])
    return nodes_right_list


def readGTFfiles(filepath):
    result = []
    with open(filepath, 'r') as myfile:
        data = myfile.read()
    data = data.split('\n')
    for line in data:
        result.append(re.split(r'\t+', line))
    return result


def compareTranslatedBinsWithGTF(TB_list, gtfFile):
    # possible TB_List
    # [['Exon0', '138027974-138028339'], ['Exon2', '138041176-138041398']]
    # possible gtfFile
    # [['chr1', 'feat', 'transcript', '137949361', '138015707', '0', '+', '.', 'gene_id "GENE.3417; transcript_id "GENE.3417"; FPKM "28.5172798216";'],
    result = []
    for exon in TB_list:
        tmp = exon.split("-")
        left_graph, right_graph = tmp[0], tmp[1]
        # left_gtf, right_gtf =

    return result


def translateBin(exons, arcs):
    result_translateBin = []
    for arc in arcs[1:]:
        edgeNumber = arc.split(" ")[2]
        tmp, seq, digits = [], [], []
        tmp = arc.split('"')
        seq = tmp[1].split(" ")
        for digit in seq[0]:
            digits.append(digit)

        for i, digit in enumerate(digits):
            if digit is "1":
                result_translateBin.append([edgeNumber, exons[i][1], exons[i][2]])
        result_translateBin.append('next')
    return result_translateBin


def translateBin2(exons, arcs):
    def combineBinsIfConnectedOptimized(pairs_stack, exon_range):
        for nodeFromStack in pairs_stack:
            if nodeFromStack[1] - 1 == exon_range[-1][2]:
                exon_range[-1] = [exon_range[-1][0], exon_range[-1][1], nodeFromStack[2]]
            else:
                exon_range.append(nodeFromStack)
        return exon_range

    def getBins(_arcs):
        result_getBins = []
        for arc in _arcs[1:]:
            edgeNumber = arc.split(" ")[2]
            _tmp, seq, digits = [], [], []
            _tmp = arc.split('"')
            seq = _tmp[1].split(" ")
            for digit in seq[0]:
                digits.append(digit)

            for i, digit in enumerate(digits):
                if digit is "1":
                    result_getBins.append([edgeNumber, exons[i][1], exons[i][2]])
            result_getBins.append('next')  # get all data
        return result_getBins

    result = []
    bins = getBins(arcs)
    while len(bins) > 1:
        edge_range = []
        if len(bins[0]) == 3:
            while len(bins[0]) == 3:
                edge_range.append(bins.pop(0))
            result.append(combineBinsIfConnectedOptimized(edge_range, [edge_range.pop(0)]))
        else:
            bins.pop(0)
    return result


def createSplicingSiteRanges(listOfExons):
    def makeListFromListOfLists(edge):
        _return = []
        for edgeNumber, start, end in edge:
            _return.extend([edgeNumber, start, end])
        return _return

    result_createSplicingSitesRanges = []
    for edge in listOfExons:
        exon_list = makeListFromListOfLists(edge)
        if len(exon_list) > 3:  # bin has more than one range
            for edgeNumber, left, right in zip(exon_list[0::3], exon_list[2::3], exon_list[4::3]):
                result_createSplicingSitesRanges.append([edgeNumber, '{}-{}'.format(left, right)])
        else:  # bin has size of one range
            result_createSplicingSitesRanges.append([edge[0][0], '{}-{}'.format(edge[0][1], edge[0][2])])
    return result_createSplicingSitesRanges


def dictToList(_transcript_dict):
    transcript_list = []
    for _, value in _transcript_dict.iteritems():
        transcript_list.append(value)
    return transcript_list


def fileFinder2(path, varType, extension):
    folders, paths2file = [], []
    for entry in os.scandir(path):
        if entry.is_dir():
            folders.append(str(entry.path) + '/' + varType + '/')

    for item in folders:
        try:
            for item2 in os.listdir(item):
                if item2.endswith(extension):
                    paths2file.append(item + item2)
        except IOError:
            continue

    return paths2file


def getWholeEdgeRangeByEdgeLabel(outgoingEdges, translatedBins):
    for whole_edge in translatedBins:
        for edges in outgoingEdges:
            if int(whole_edge[0][0]) in edges:
                yield whole_edge


def getWholeEdgeRangeAsList(edge_label, translatedBins):
    for whole_edge in translatedBins:
        if int(whole_edge[0][0]) == int(edge_label):
            return [[start, end] for label, start, end in whole_edge]


def getTranscripts(workingBase, arcs, translatedBins, fromNode=0, possibleTranscripts=set()):
    print('next run')
    outgoing_paths = getEdgesFromNode(fromNode, arcs)
    for edge_range in getWholeEdgeRangeByEdgeLabel(outgoing_paths, translatedBins):
        for transcript in workingBase:
            print(transcript)
            edge_label = edge_range[0][0]
            checkRange = getWholeEdgeRangeAsList(edge_label, translatedBins)
            # if len(transcript) == len(checkRange):
            #    print('we have here {} and {}'.format(transcript[1], checkRange[0:len(checkRange)]))
            if transcript[1][1:-1] == checkRange[1:-1] or transcript == checkRange:
                # print('edge matches: {}'.format(edge_range))
                node_right = int(outgoing_paths[0][2])
                node_left = int(outgoing_paths[0][0])
                edge_len = len(edge_range) - 1
                possibleTranscripts.add(str([transcript[edge_len:], [node_left, edge_label, node_right]]))
                possibleTranscripts_list = list(possibleTranscripts)
                getTranscripts(possibleTranscripts_list, node_right, possibleTranscripts)
    return possibleTranscripts


def checkTranscripts_proto(working_set, start_node=0, transcript_set=set()):
    outgoing_paths = getEdgesFromNode(start_node, cg_arcs)  # [0,41,22] - [left,label,right]
    right_node = int(outgoing_paths[0][2])
    left_node = int(outgoing_paths[0][0])
    if right_node is not 0:
        for edge_range in getWholeEdgeRangeByEdgeLabel(outgoing_paths, translatedBins):
            print('edge range is: {}'.format(edge_range))
            for _transcript in working_set:
                print(_transcript)
                # here i need to check, whether we are in the begin, middle or end of transcript
                right_site_from_first_exon_transcript = int(_transcript[1][0].split('-')[1])
                right_site_from_first_exon_graph = edge_range[0][2]
                if right_site_from_first_exon_transcript == right_site_from_first_exon_graph:
                    edge_label = edge_range[0][0]
                    number_of_exons_on_edge = len(edge_range) - 1
                    if len(_transcript[1]) >= number_of_exons_on_edge:
                        range_at_edge_end_transcript = _transcript[1][number_of_exons_on_edge]
                        tmp = str(edge_range[number_of_exons_on_edge][1:])
                        range_at_edge_end_graph = tmp.replace('[', '').replace(']', '').replace(', ', '-')
                        #  print("the ranges transcript {} and graph are {}".format(range_at_edge_end_transcript, range_at_edge_end_graph))
                        if range_at_edge_end_transcript == range_at_edge_end_graph:
                            _transcript = list(_transcript)
                            _transcript.append([left_node, edge_label, right_node])
                            transcript_set.add(str(_transcript))
                            _workingOnEdges = list(transcript_set)
                            checkTranscripts_proto(_workingOnEdges, left_node, transcript_set)
    else:
        return transcript_set
