import os
import re

path = r'C:\Users\dusty\PycharmProjects\graphenprakt\True\true_alignment.bam.chr1.138364543.138424370\unfiltered\chr1_0.graph'


def fileFinder2(path, varType, extension):
    folders, paths2file = [], []
    for entry in os.scandir(path):
        if entry.is_dir():
            folders.append(str(entry.path) + '/' + varType + '/')

    for item in folders:
        try:
            for item2 in os.listdir(item):
                if item2.endswith(extension):
                    paths2file.append(item + item2)
        except IOError:
            continue
    return paths2file


def getArcs(path):
    with open(path, 'r') as myfile:
        data = myfile.read().replace('\n', '|')
    data = re.split('Compacted graph ---(.*?)Resolved graph ---', data)
    tmp_list = []
    tmp_tmp = re.split('@arcs(.*?)@attributes', data[1].replace('\t', ' '))
    tmp = tmp_tmp[1].split('|')
    for unit in tmp:
        if unit != "":
            tmp_list.append(unit.split(' ')[:-1])
    return tmp_list[1:]


def extractTranscripts_listOfRanges(path=None):
    if path is None:
        #    path = r'C:\Users\dusty\PycharmProjects\graphenprakt\True\true_alignment.bam.chr1.138364543.138424370\truth.gtf'
        path = r'C:\Users\dusty\PycharmProjects\graphenprakt\True\true_alignment.bam.chr1.138364543.138424370\unfiltered\transcripts.gtf'

    num_lines = sum(1 for line in open(path))
    with open(path, 'r') as myfile:
        transcript_builder, transcripts = [], []
        for i, line in enumerate(myfile):
            if line.split('\t')[2] == 'exon':
                transcript_builder.append([splits for splits in line.split("\t") if splits is not ""])
            else:
                if line.split('\t')[6] == '-' and predictedOrTruth == 'truth':
                    transcript_builder = transcript_builder[::-1]
                transcripts.append(transcript_builder)
                transcript_builder = []
            if i == num_lines - 1:
                if line.split('\t')[6] == '-' and predictedOrTruth == 'truth':
                    transcript_builder = transcript_builder[::-1]
                transcripts.append(transcript_builder)

    result = []
    for transcript in transcripts[1:]:
        # print('transcript %s' %str(transcript))
        # print('transcript6 %s' %str(transcript[0][6]))
        transcript_builder = []
        for item in transcript:
            transcript_builder.extend([int(item[3]), int(item[4])])
        # print(transcript_builder)
        # print('strand is %s' % strand)

        result.append(transcript_builder)
    return result


def getOutgoingEdgesByNode(node):
    def translateBin(binaryBin):
        def combineExonsIfPossible(pairs_stack, exon_range):
            for nodeFromStack in pairs_stack:
                if nodeFromStack[0] - 1 == exon_range[-1][1]:
                    exon_range[-1] = [exon_range[-1][0], nodeFromStack[1]]
                else:
                    exon_range.append(nodeFromStack)
            yield exon_range

        def getExons(path):
            with open(path, 'r') as myfile:
                data = myfile.read().replace('\n', '|')
            exon_list = []
            exons_tmp = re.split('Exon Lists ---(.*?)Multi Bins ---', data)
            exons = exons_tmp[1].split('|')
            for exon in exons:
                if exon != "":
                    exon = exon.split(" ")
                    tmp = exon[1].split("-")
                    left, right = int(tmp[0]), int(tmp[1])
                    exon_list.append([int(exon[0].replace("Exon", "")), left, right])
            return exon_list

        digits = [digit for digit in binaryBin]
        exons = getExons(path)
        result_getBins = []
        for i, digit in enumerate(digits[1:]):
            if digit is "1":
                result_getBins.append([exons[i][1], exons[i][2]])

        edge_range, result = [], []
        binaryBin = result_getBins

        for item in combineExonsIfPossible(binaryBin, [binaryBin.pop(0)]):
            result.extend(item)

        returnList = []
        _builder = []

        for item in result:
            _builder.extend([item[0], item[1]])
        returnList.extend(_builder)

        return returnList

    for left, right, label, transBin, _, edgeType, coverage in arcs:
        if int(left) == int(node):
            yield left, right, label, translateBin(transBin), edgeType, coverage


def checkTranscriptRangesForMatchingSetOfEdges(transcript, startNode=0, potentialTranscriptPath=[], returnPath=[]):
    # what if we cannot find any transcript matching?
    if startNode == 1 and len(transcript) == 2:  # we must be at the end, next node is end at transcript has no leftover
        returnPath.append(potentialTranscriptPath)
    else:
        if len(transcript) > 1:
            for edge in getOutgoingEdgesByNode(startNode):
                currPath = potentialTranscriptPath[:]
                nextNode, edgeLen = int(edge[1]), len(edge[3])
                if edge[3][1:-1] == transcript[1:edgeLen - 1]:
                    currPath.append(edge)
                    checkTranscriptRangesForMatchingSetOfEdges(transcript[edgeLen - 2:], nextNode, currPath, returnPath)
    return returnPath


def transcriptsToEdgePaths(transcripts):
    for transcript in transcripts:
        if len(transcript) > 2:
            tmp = checkTranscriptRangesForMatchingSetOfEdges(transcript, _pathForTranscript=[], paths=[])
            if (len(tmp)) > 1:
                print('problem here, len of tmp is not cool')
                exit(1)
            path_builder = []
            for item in tmp:
                for _, _, edge, _, _, _ in item:
                    path_builder.extend([int(edge)])
                print(path_builder)


###############################################################################################
filteredOrUnfiltered = 'unfiltered'
predictedOrTruth = 'predicted'  # 'predicted'
###############################################################################################

notFoundTranscripts = open('notFoundTranscripts' + filteredOrUnfiltered + '.txt', 'w')
foundTranscripts = open('foundTranscripts' + filteredOrUnfiltered + '.txt', 'w')

# notFoundTruths = open('notFoundTruths.txt','w')
# foundTruths = open('foundTruths.txt','w')
listOfFiles = fileFinder2(r'C:/Users/dusty/PycharmProjects/graphenprakt/True', filteredOrUnfiltered, '.graph')
i, z = 0, 0

found_dict = {}
verify_found_dict = {}
i += 1

for path in listOfFiles:
    # i += 1
    # print(str(i) + " " + path)
    # arcs = getArcs(path)
    # path_predicted = re.sub('/chr(.*?).graph', '/transcripts.gtf', path)
    # path_truth = re.sub('/' + filteredOrUnfiltered + '/(.*?).graph', '/truth.gtf', path)

    # actual_folder = str(path).split('\\')[1]
    # print(actual_folder)

    # predictedTranscripts_ListOfListsWithRanges = extractTranscripts_listOfRanges(path_predicted)
    # # print('%d transcripts in "%s" found' % (len(predictedTranscripts_ListOfListsWithRanges), str(path).split('\\')[1]))
    #
    # predictedTranscripts_listOfLists, foundListOfEdgesForTranscript = [], []
    # for predictedTranscript_ListOfRanges in predictedTranscripts_ListOfListsWithRanges:
    #     # print('looking at transcript %s' % predictedTranscript_ListOfRanges)
    #     if str(predictedTranscript_ListOfRanges) not in found_dict or not found_dict[
    #         str(predictedTranscript_ListOfRanges)]:
    #         found_dict[str(predictedTranscript_ListOfRanges)] = False
    #     foundListOfEdgesForTranscript = checkTranscriptRangesForMatchingSetOfEdges(predictedTranscript_ListOfRanges, 0,
    #                                                                                [], [])
    #     if len(foundListOfEdgesForTranscript) == 1:
    #         found_dict[str(predictedTranscript_ListOfRanges)] = True
    #         # print('found edge %s' % foundListOfEdgesForTranscript)
    #         predictedTranscripts_listOfLists.append(foundListOfEdgesForTranscript)
    #         foundTranscripts.write('%s\t%s\t%s\n' % (
    #             str(path).split('\\')[1], str(predictedTranscript_ListOfRanges), foundListOfEdgesForTranscript))
    #     # foundTranscripts.write('for the Range: %s \n' % str(predictedTranscript_ListOfRanges))
    #     #  foundTranscripts.write('path of transcript: %s \n' % foundListOfEdgesForTranscript)
    #     else:
    #         notFoundTranscripts.write('%s\t%s \n' % (str(path).split('\\')[1], predictedTranscript_ListOfRanges))

    #####
    # to do: if MINUS-STRAND, need to REVERSE to the 
    #####

    # trueTranscripts_ListOfListsWithRanges = extractTranscripts_listOfRanges(path_truth)
    # # print('%d transcripts in "%s" found' % (len(predictedTranscripts_ListOfListsWithRanges), str(path).split('\\')[1]))
    # trueTranscripts_listOfLists, foundListOfEdgesForTranscript = [], []
    # for trueTranscript_ListOfRanges in trueTranscripts_ListOfListsWithRanges:
    #     # print('looking at transcript %s' % predictedTranscript_ListOfRanges)
    #     if str(trueTranscript_ListOfRanges) not in found_dict or not found_dict[
    #         str(trueTranscript_ListOfRanges)]:
    #         found_dict[str(trueTranscript_ListOfRanges)] = False
    #     foundListOfEdgesForTranscript = checkTranscriptRangesForMatchingSetOfEdges(trueTranscript_ListOfRanges, 0,
    #                                                                                [], [])
    #     if len(foundListOfEdgesForTranscript) == 1:
    #         found_dict[str(trueTranscript_ListOfRanges)] = True
    #         # print('found edge %s' % foundListOfEdgesForTranscript)
    #         trueTranscripts_listOfLists.append(foundListOfEdgesForTranscript)
    #         foundTranscripts.write('%s\t%s\t%s\n' % (
    #             str(path).split('\\')[1], str(trueTranscript_ListOfRanges), foundListOfEdgesForTranscript))
    #     # foundTranscripts.write('for the Range: %s \n' % str(predictedTranscript_ListOfRanges))
    #     #  foundTranscripts.write('path of transcript: %s \n' % foundListOfEdgesForTranscript)
    #     else:
    #        # print(str(path).split('\\'))
    #         notFoundTruths.write('%s\t\%s \n' % ((str(path).split('\\')[1]), trueTranscript_ListOfRanges))

print('number of existent transcripts %d' % len(found_dict))

n = 0
for key, value in found_dict.items():
    if len(key.split(' ')) > 2:
        n += 1

print('we could only possible find %d transcripts bc 2>exons' % n)

z = 0
for key, value in found_dict.items():
    if value is True:
        z += 1

print('#foundPaths %d' % z)

# for key, value in found_dict.items():
#     if value is False and len(key.split(' ')) > 2:
#         print(key,value)

notFoundTranscripts.close()
foundTranscripts.close()
