import re
import os

'''
filepath = os.getcwd() + "/PycharmProjects/graphenpraktReal/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/splice_graph/transcripts.gtf"
with open(filepath, 'r') as myfile:
    data = myfile.read().replace('\n', '|').replace('\t', '--')
    data = re.replace('chr1--ryuto--transcript', '------transcript')
transcriptList = re.split('chr1|ryuto|transcript'(.*?)'chr1|ryuto|transcript', data)
'''


def extractTranscriptsSplicingSitesConnected():
    filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/filtered/transcripts.gtf"
    with open(filepath, 'r') as myfile:
        data = myfile.read().replace('\n', '|').replace('\t', '--')
        data = data.replace('chr1--ryuto--transcript', '------transcript')
    transcriptList = re.split('------(.*?)transcript', data)
    transcriptDict = {}
    dict_key = 0
    for line in transcriptList:
        if len(line) > 0:
            transcript = line.split('|')
            # print transcript
            # geInf = transcript[0].split('--')[1] + "-" + transcript[0].split('--')[2]  # use as the key of the dict
            exon_range = []
            for i, inf in enumerate(transcript[1:]):
                # for inf in transcript[1:]:
                if len(inf) > 0:
                    # print inf.split('--')
                    # exonPos = inf.split('--')[3] + "-" + inf.split('--')[4]
                    exonPos = transcript[i].split('--')[4] + "-" + transcript[i + 1].split('--')[3]
                    exon_range.append(exonPos)
            transcriptDict[dict_key] = exon_range
            dict_key += 1
    return transcriptDict


def extractTranscripts():
    #  filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/filtered/transcripts.gtf"
    filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/unfiltered/transcripts.gtf"

    with open(filepath, 'r') as myfile:
        data = myfile.read().replace('\n', '|').replace('\t', '--')
        data = data.replace('chr1--ryuto--transcript', '------transcript')
    transcriptList = re.split('------(.*?)transcript', data)
    transcriptDict = {}
    dict_key = 0
    for line in transcriptList:
        if len(line) > 0:
            transcript = line.split('|')
            # print transcript
            # geInf = transcript[0].split('--')[1] + "-" + transcript[0].split('--')[2]  # use as the key of the dict
            exon_range = []
            for i, inf in enumerate(transcript[1:]):
                # for inf in transcript[1:]:
                if len(inf) > 0:
                    # print inf.split('--')
                    exonPos = inf.split('--')[3] + "-" + inf.split('--')[4]
                    # exonPos = transcript[i].split('--')[3] + "-" + transcript[i + 1].split('--')[3]
                    exon_range.append(exonPos)
            transcriptDict[dict_key] = exon_range
            dict_key += 1
    return transcriptDict


def extractTranscripts_list(path):
    #  filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/filtered/transcripts.gtf"
    # filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/unfiltered/transcripts.gtf"

    with open(path, 'r') as myfile:
        data = myfile.read().replace('\n', '|').replace('\t', '--')
        data = data.replace('chr1--ryuto--transcript', '------transcript')
    transcriptList = re.split('------(.*?)transcript', data)
    transcript_list = []
    for line in transcriptList:
        print(line)
        if len(line) > 0:
            transcript = line.split('|')
            for i, inf in enumerate(transcript[1:]):
                if len(inf) > 5:
                    exonPos = inf.split('--')[3] + "-" + inf.split('--')[4]
                    transcript_list.append(exonPos)
    return transcript_list


def extractTranscriptStringSplicingSites():
    filepath = os.getcwd() + "/star_without_gene_models.bam.chr1.137949360.138130838-20180205T123104Z-001/filtered/transcripts.gtf"
    with open(filepath, 'r') as myfile:
        data = myfile.read().replace('\n', 'newLine').replace('\t', ' ')

    transcriptList = re.split('chr(.*?);newLine', data)

    tmp = []
    for item in transcriptList:
        if len(item) > 0:
            tmp.append(item)
    transcriptList = tmp

    exon_range = []
    for i, listElement in enumerate(transcriptList):
        # print(listElement)
        if len(listElement) > 0 and i + 1 < len(transcriptList):
            exonPos = transcriptList[i].split(' ')[4] + "-" + transcriptList[i + 1].split(' ')[3]
            transcriptList[i] += ' ' + exonPos
            exon_range.append(transcriptList[i])
    return exon_range
