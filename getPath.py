from Task1_functions import *

def getAjaMat(data):
    arcs = getArcs(data)
    arcs_list = []
    for line in arcs[1:]:
        tmp = line.replace('"', '').split(" ")
        arcs_list.append([tmp[0],tmp[1]])
    nodes = getLabels(data)
    print arcs_list
    print nodes
    m = len(nodes)
    #initial AjaMat
    AM = [[0 for x in range(m)]for y in range(m)]
    for arc in arcs_list:
        inNode, outNode = arc[0], arc[1]
        x,y = nodes.index(arc[0]), nodes.index(arc[1])
        AM[x][y] += 1
    return AM

def getInNode(label, arcs):
    for line in arcs:
        if line[2] == label:
            return line[0]
    return False

def getOutNode(label, arcs):
    for line in arcs:
        if line[2] == label:
            return line[1]
    return False

def intNodes(nodes):
    intNodes = []
    for node in nodes:
        intNodes.append(int(node))
    return intNodes

def getPath(AM, nodes, arcsOri):
    arcs = []
    for line in arcsOri[1:]:
        arc = line.replace('"', '').split(" ")
        arcs.append(arc)
    path_list = []
    path_list_cur = []
    pathList = []
    # in the graph node 0 is always the source and node 1 is always the drain
    # first find the last edge in the path
    mystack = set()
    cur_index = nodes.index("1")
    for x in range(len(nodes)):
        if AM[x][cur_index] != 0:
            mystack.add(nodes[x])
    print mystack
    for line in arcs:
        #print line[0], line[1]
        if line[0] in mystack and line[1] == "1":
            path = [line[2]]
            path_list_cur.append(path)
    curNode = mystack
    mystack = set()
    path_list = path_list_cur
    path_list_cur = []

    for path in path_list:
        if getInNode(path[-1], arcs) == "0" and path not in pathList:
            pathList.append(path)
    #find the next edge in the path
    while len(curNode) > 0:
        print ("curNode: ")
        print curNode
        for node in curNode:
            if node != "0":
                cur_index = nodes.index(node)
                for x in range(len(nodes)):
                    if AM[x][cur_index] != 0:
                        mystack.add(nodes[x])
                for next_node in mystack:
                    for line in arcs:
                        if next_node == line[0] and node == line[1]:
                            for path in path_list:
                                print path
                                if getInNode(path[-1], arcs) == node:
                                    newPath = path + [line[2]]
                                    path_list_cur.append(newPath)
            else:
                for path in path_list:
                    if getInNode(path[-1], arcs) == "0" and path not in pathList:
                        pathList.append(path)
        curNode = mystack
        mystack = set()
        path_list = path_list_cur
        path_list_cur = []

    print pathList
    pathListInOrder = []
    for path in pathList:
        path.reverse()
        pathListInOrder.append(path)

    return pathListInOrder







