import re

filteredOrUnfiltered = 'unfiltered'
#truthOrNot = 'transcripts'

pathVariable = r'C:\\Users\\dusty\\PycharmProjects\\graphenprakt\\True\\'
truthDataPath = r'C:\\Users\\dusty\\PycharmProjects\\graphenprakt\\results\\' + filteredOrUnfiltered + 'Truth\\foundTruths.txt'
transcriptDataPath = r'C:\\Users\\dusty\\PycharmProjects\\graphenprakt\\results\\' + filteredOrUnfiltered + 'Predicted\\foundTranscripts' + filteredOrUnfiltered + '.txt'
resultDict = {}


def getArcs(_path, graph_type):
    with open(_path, 'r') as myfile:
        data = myfile.read().replace('\n', '|')
    if graph_type == 'cg':
        data = re.split('Compacted graph ---(.*?)Resolved graph ---', data)
    elif graph_type == 'rg':
        data = re.split('Resolved graph ---(.*?)source', data)
    elif graph_type == 'ig':
        data = re.split('Initial Graph ---(.*?)Compacted graph ---', data)
    tmp_list = []
    tmp_tmp = re.split('@arcs(.*?)@attributes', data[1].replace('\t', ' '))
    tmp = tmp_tmp[1].split('|')
    for unit in tmp:
        if unit != "":
            tmp_list.append(unit.split(' ')[:-1])
    return tmp_list[1:]


def calcExonDegrees(_arcs, degreeType, problemNodes=None):
    nodes_1, nodes_2, nodes_4, arc_list = [], [], [], []

    # for line in arcs:
    #     tmp = line.replace('"', '').split(" ")
    #     arc_list.append(tmp)
    # arc_list = arc_list[1:]
    for _node_in, _node_out, _node_label, _node_bin, _node_resolve, _node_type, _node_evidence in _arcs:
        _node_type = int(_node_type)
        if _node_type is 1:
            #  nodes_1.append((_node_in, _node_out))
            nodes_1.append((_node_in, _node_out, _node_evidence))
        elif _node_type is 2:
            nodes_2.append((_node_in, _node_out, _node_evidence))
        elif _node_type is 4:
            nodes_4.append((_node_in, _node_out, _node_evidence))
        else:
            print('got no type ?!')
    if len(nodes_2) >= 1:  # that's valid for InitialGraph
        nodes_right_list, nodes_left_list = [], []

        for node_left, node_right, node_evidence in nodes_1:
            nodes_right_list.append(node_right)
            nodes_left_list.append(node_left)
        for node_left, node_right, node_evidence in nodes_4:
            nodes_right_list.append(node_right)
            nodes_left_list.append(node_left)

        inDegree, outDegree = set(), set()

        for node_left, node_right, node_evidence in nodes_1 + nodes_4:
            outDegree.add((node_left, nodes_left_list.count(node_left), node_evidence))
            inDegree.add((node_right, nodes_right_list.count(node_right), node_evidence))
            if node_left in nodes_right_list:
                outDegree.add((node_left, nodes_right_list.count(node_right), node_evidence))
            elif node_right in nodes_left_list:
                inDegree.add((node_right, nodes_left_list.count(node_left), node_evidence))
            else:
                for node_left2, node_right2, node_evidence in nodes_2:
                    if node_left == node_right2:
                        inDegree.add((node_left, nodes_right_list.count(node_left2), node_evidence))
                    if node_right == node_left2:
                        outDegree.add((node_right, nodes_left_list.count(node_right2), node_evidence))
        outDegree.add(('0', nodes_left_list.count('0'), 'unknown'))
        inDegree.add(('1', nodes_right_list.count('1'), 'unknown'))

    else:  # that's valid for Resolved and compressed Graph
        nodes_right_1_list, nodes_left_1_list = [], []
        for node_left, node_right in nodes_1:
            nodes_right_1_list.append(node_right)
            nodes_left_1_list.append(node_left)
        inDegree, outDegree = set(), set()
        for node_left1, node_right1 in nodes_1:  # look at all tuples in nodes_1
            outDegree.add((node_left1, nodes_right_1_list.count(node_right1)))
            inDegree.add((node_right1, nodes_left_1_list.count(node_left1)))
        outDegree.add(('0', nodes_left_1_list.count('0')))
        inDegree.add(('1', nodes_right_1_list.count('1')))

    if problemNodes is None:
        if degreeType == 'out':
            return outDegree
        elif degreeType == 'in':
            return inDegree
    else:
        if degreeType == 'out':
            outDegreeFiltered = set()
            for _node, count, fpkm in outDegree:
                if int(_node) in problemNodes:
                    outDegreeFiltered.add((_node, count, fpkm))
            return outDegreeFiltered
        if degreeType == 'in':
            inDegreeFiltered = set()
            for _node, count, fpkm in inDegree:
                if int(_node) in problemNodes:
                    inDegreeFiltered.add((_node, count, fpkm))
            return inDegreeFiltered


with open(truthDataPath, 'r') as truthFile:
    for line in truthFile:
        splitted_line = line.split('\t')
        assert len(splitted_line) == 3
        fileName = splitted_line[0]
        path = splitted_line[2].replace('[[(', '').replace(')]]\n', '').split('),')
        if fileName not in resultDict:
            resultDict[fileName] = [[path], []]
        else:
            paths = resultDict[fileName][0][:]
            paths.append(path)
            resultDict[fileName] = [paths, []]

# for key in resultDict:
#    print(key, resultDict[key])

with open(transcriptDataPath, 'r') as predictedFile:
    for line in predictedFile:
        splitted_line = line.split('\t')
        assert len(splitted_line) == 3
        fileName = splitted_line[0]
        path = splitted_line[2].replace('[[(', '').replace(')]]\n', '').split('),')
        # print(path)
        if fileName not in resultDict:

            resultDict[fileName] = [[], [path]]
        else:
            # old_value = list(resultDict[fileName])
            # new_value = old_value.append([path], [])
            paths = resultDict[fileName][1][:]
            #   print(paths)
            truthPaths = resultDict[fileName][0][:]
            # print(truthPaths)
            paths.append(path)
            resultDict[fileName] = [truthPaths, paths]

boringCounterLalaLu = 0
list_OfFPKMValues = []
for key in resultDict:
    boringCounterLalaLu += 1
    print(boringCounterLalaLu, key)
    list_builder = []
    node_list = []
    ListOfTruthPaths = resultDict[key][0][:]
    ListOfPredictedPaths = resultDict[key][1][:]
    if len(ListOfTruthPaths) > 2:
        ListOfTruthPathsForBreakingPoints, ListOfPredictedPathsForBreakingPoints = [], []
        ListOfCandidateChimaeren, ListOfTruthEdge, ListOfChimaeren, ListOfTruePredicted = [], [], [], []
        for ListOfStringEdgeOfTruthPath in ListOfTruthPaths:
            ListOfIntEdgeOfTruthPath = []
            for StringEdge in ListOfStringEdgeOfTruthPath:
                edgeNum = int(StringEdge.split(',')[2].replace("'", ''))
                ListOfIntEdgeOfTruthPath.append(edgeNum)
                ListOfTruthEdge.append(edgeNum)
            ListOfTruthPathsForBreakingPoints.append(ListOfIntEdgeOfTruthPath)

        for ListOfStringEdgeOfPredictedPath in ListOfPredictedPaths:
            ListOfIntEdgeOfPredictedPath = []
            edgeCounter = 0
            for StringEdge in ListOfStringEdgeOfPredictedPath:
                edgeNum = int(StringEdge.split(',')[2].replace("'", ''))
                ListOfIntEdgeOfPredictedPath.append(edgeNum)
                if edgeNum in ListOfTruthEdge:
                    edgeCounter += 1
            ListOfPredictedPathsForBreakingPoints.append(ListOfIntEdgeOfPredictedPath)
            # print('edge counter: %d' %edgeCounter)
            # print('listLen: %d' % len(ListOfStringEdgeOfPredictedPath))
            if edgeCounter == len(ListOfStringEdgeOfPredictedPath):
                ListOfCandidateChimaeren.append(ListOfIntEdgeOfPredictedPath)
        # (ListOfTruthPathsForBreakingPoints)

        for ListOfIntEdgeOfCandidateChimaeren in ListOfCandidateChimaeren:
            # print(ListOfIntEdgeOfCandidateChimaeren, ListOfTruthPathsForBreakingPoints)
            if ListOfIntEdgeOfCandidateChimaeren in ListOfTruthPathsForBreakingPoints:
                ListOfTruePredicted.append(ListOfIntEdgeOfCandidateChimaeren)
            else:
                ListOfChimaeren.append(ListOfIntEdgeOfCandidateChimaeren)
        # print(ListOfChimaeren)

        #################################################################################
        ####################### PSEUDO ALGORITHM ########################################
        #################################################################################
        finallySolved = []
        myfinally = []
        for ListOfIntEdgeOfChimaer in ListOfChimaeren:
            candidatesOfBreakingPoints = []
            for i, ListOfIntEdgeOfTruthPredictedPath in enumerate(ListOfTruthPathsForBreakingPoints):
                if ListOfIntEdgeOfTruthPredictedPath[0] == ListOfIntEdgeOfChimaer[0]:
                    candidatesOfBreakingPoints.append(([i], [], 0))
            for k in range(1, len(ListOfIntEdgeOfChimaer)):
                next_candidatesOfBreakingPoints = []
                for i, ListOfIntEdgeOfTruthPredictedPath in enumerate(ListOfTruthPathsForBreakingPoints):
                    new = []
                    for l in range(1, len(ListOfIntEdgeOfTruthPredictedPath)):
                        if ListOfIntEdgeOfTruthPredictedPath[l] == ListOfIntEdgeOfChimaer[k]:
                            for c in candidatesOfBreakingPoints:
                                if c[0][-1] == i:
                                    new.append((c[0], c[1], c[2]))
                                else:
                                    # print(c[1], [k])
                                    # print(c[1] + [k])
                                    new.append((c[0] + [i], c[1] + [k], c[2] + 1))
                            break
                    if new:
                        # print('new')
                        # print(new)
                        new_min = min(u[2] for u in new)
                        for n in new:
                            if n[2] == new_min:
                                next_candidatesOfBreakingPoints.append(n)
                candidatesOfBreakingPoints = next_candidatesOfBreakingPoints
            final = []
            if candidatesOfBreakingPoints:
                can_min = min(u[2] for u in candidatesOfBreakingPoints)
                for n in candidatesOfBreakingPoints:
                    if n[2] == can_min:
                        final.append(n)
            myfinally.append([ListOfIntEdgeOfChimaer, final[0][1]])
            finallySolved.append(final)
        ListOfProblemEdges = []

        if myfinally:
            # print(myfinally)
            for eachChimaerBreakPoints in myfinally:
                ListOfEdgeOfChimaer, ListOfBreakPoints = eachChimaerBreakPoints[0], eachChimaerBreakPoints[1]
                for index in ListOfBreakPoints:
                    ListOfProblemEdges.append(ListOfEdgeOfChimaer[index])
            filePath = pathVariable + str(key)
            arcs = getArcs(filePath, 'rg')
            DictOfProblemNodesAndCount = {}
            for arc in arcs:
                if int(arc[2]) in ListOfProblemEdges:
                    if arc[0] not in DictOfProblemNodesAndCount:
                        DictOfProblemNodesAndCount[arc[0]] = 1
                    else:
                        DictOfProblemNodesAndCount[arc[0]] = DictOfProblemNodesAndCount[arc[0]] + 1
                    nodeCount = DictOfProblemNodesAndCount[arc[0]]
            # need outDegree of left Node
            for node in DictOfProblemNodesAndCount:
                node_list.append(int(node))
            relevantEdges = calcExonDegrees(getArcs(filePath, 'ig'), 'in', node_list)
            for item in relevantEdges:
                list_OfFPKMValues.append(int(item[2]))
print(list_OfFPKMValues)
# 12,15,20,40
# getLeftNodesFromEdges(list)
# node node node node

# print('############################################')
